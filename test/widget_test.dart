import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/models/articulo.dart';
import 'package:cuarentime_app/models/categoria.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/usuario2.dart';
import 'package:cuarentime_app/models/video.dart';
import 'package:intl/intl.dart';
import 'package:test/test.dart';

// Total de pruebas = 40

void main() {
  // Pruebas Nombre categoria
  test('Test Nombre de categorias -> Retornar nombre correcto ', () {
    Categoria categoria1 = Categoria(id: "1", nombre: "Cocina");
    Categoria categoria2 = Categoria(id: "2", nombre: "Educacion");
    Categoria categoria3 = Categoria(id: "3", nombre: "Deportes");
    List<Categoria> categorias = [categoria1, categoria2, categoria3];
    if (categorias != null) {
      for (var i = 0; i < categorias.length; i++) {
        if ("1" == categorias[i].id) {
          expect(categorias[i].nombre, "Cocina");
        }
      }
    }
    expect("No se encontro categorias", isNotNull);
  });

  // Pruebas Formato fecha
  test('Test Formato fecha -> Retorar formato fecha', () {
    var timestamp = Timestamp.now();
    var date =
        DateTime.fromMillisecondsSinceEpoch(timestamp.microsecondsSinceEpoch);
    expect(date, isNotNull);
  });

  // Pruebas Formato dinero
  test('Test Formato dinero', () {
    var formato = NumberFormat('#,###');
    String dinero = "2000";
    expect(formato.format(int.parse(dinero)), "2,000");
  });

  // Pruebas Paquete
  Paquete paqueteNormal = Paquete(
    id: "1",
    titulo: "Clase de yoga",
    resumen: "Lorem Ipsum is simply dummy text of the printing",
    imagen: "assets/images/home/yoga.jpg",
    videos: [],
    comentarios: [],
    precio: "3000",
    categoria: "2",
    usuario: "2",
    aprobado: "",
    publicacion: Timestamp.now(),
    publicado: false,
  );
  test('Test modelo Paquete -> Paquete vacio', () {
    Paquete paquete;
    expect(paquete, null);
  });
  test('Test modelo Paquete -> Paquete completo', () {
    expect(paqueteNormal, isNotNull);
  });
  test('Test modelo Paquete -> Titulo del paquete', () {
    expect(paqueteNormal.titulo, isNotNull);
  });
  test('Test modelo Paquete -> Resumen del paquete', () {
    expect(paqueteNormal.resumen, isNotNull);
  });
  test('Test modelo Paquete -> Imagen del paquete', () {
    expect(paqueteNormal.imagen, isNotNull);
  });
  test('Test modelo Paquete -> Paquete sin videos ', () {
    expect(paqueteNormal.videos.length, 0);
  });
  test('Test modelo Paquete -> Paquete sin comentarios ', () {
    expect(paqueteNormal.comentarios.length, 0);
  });
  test('Test modelo Paquete -> Precio del paquete numerico', () {
    expect(int.parse(paqueteNormal.precio), 3000);
  });
  test('Test modelo Paquete -> Categoria del paquete', () {
    expect(paqueteNormal.categoria, isNotNull);
  });
  test('Test modelo Paquete -> Dueño del paquete', () {
    expect(paqueteNormal.usuario, isNotNull);
  });
  test('Test modelo Paquete -> Paquete no aprobado ', () {
    expect(paqueteNormal.aprobado, "");
  });
  test('Test modelo Paquete -> Paquete no publicado ', () {
    expect(paqueteNormal.publicado, false);
  });
  test('Test modelo Paquete -> Fecha del paquete', () {
    expect(paqueteNormal.publicacion, isNotNull);
  });

  // Pruebas Video
  Video videoNormal = Video(
    id: "1",
    titulo: "Primera clase de yoga",
    descripcion: "Lorem Ipsum is simply dummy text of the printing",
    imagen: "assets/images/home/yoga.jpg",
    video: "",
    paquete: "1",
    usuario: "1",
  );
  test('Test modelo Video -> Video vacio', () {
    Video video;
    expect(video, null);
  });
  test('Test modelo Video -> Video completo', () {
    expect(videoNormal, isNotNull);
  });
  test('Test modelo Video -> Titulo del video', () {
    expect(videoNormal.titulo, isNotNull);
  });
  test('Test modelo Video -> Descripcion del video', () {
    expect(videoNormal.descripcion, isNotNull);
  });
  test('Test modelo Video -> Imagen del video', () {
    expect(videoNormal.imagen, isNotNull);
  });
  test('Test modelo Video -> Video del video', () {
    expect(videoNormal.video, isNotNull);
  });
  test('Test modelo Video -> Dueño del video', () {
    expect(videoNormal.usuario, isNotNull);
  });
  test('Test modelo Video -> Paquete del video', () {
    expect(videoNormal.paquete, isNotNull);
  });

  // Pruebas Articulo
  Articulo articuloNormal = Articulo(
    id: "1",
    titulo: "Como hacer una clase de yoga",
    contenido: "Lorem Ipsum is simply dummy text of the printing",
    imagen: "assets/images/home/yoga.jpg",
    categoria: "1",
    usuario: "1",
  );
  test('Test modelo Articulo -> Articulo vacio', () {
    Articulo articulo;
    expect(articulo, null);
  });
  test('Test modelo Articulo -> Articulo completo', () {
    expect(articuloNormal, isNotNull);
  });
  test('Test modelo Articulo -> Titulo del articulo', () {
    expect(articuloNormal.titulo, isNotNull);
  });
  test('Test modelo Articulo -> Contenido del articulo', () {
    expect(articuloNormal.contenido, isNotNull);
  });
  test('Test modelo Articulo -> Imagen del articulo', () {
    expect(articuloNormal.imagen, isNotNull);
  });
  test('Test modelo Articulo -> Dueño del articulo', () {
    expect(articuloNormal.usuario, isNotNull);
  });
  test('Test modelo Articulo -> Categoria del articulo', () {
    expect(articuloNormal.categoria, isNotNull);
  });

  // Prueba Usuario
  Usuario2 usuarioNormal = Usuario2(
    id: "",
    nombre: "",
    imagen: "",
    paquetes: [],
    paquetesComprados: [],
    tipo: false,
    usuario: "",
    ventasTotales: 0,
  );

  test('Test modelo Usuario -> Usuario vacio', () {
    Usuario2 usuario;
    expect(usuario, null);
  });
  test('Test modelo Usuario -> Usuario completo', () {
    expect(usuarioNormal, isNotNull);
  });
  test('Test modelo Usuario -> Nombre del usuario', () {
    expect(usuarioNormal.nombre, isNotNull);
  });
  test('Test modelo Usuario -> Imagen del usuario', () {
    expect(usuarioNormal.imagen, isNotNull);
  });
  test('Test modelo Usuario -> Usuario sin paquetes', () {
    expect(usuarioNormal.paquetes.length, 0);
  });
  test('Test modelo Usuario -> Usuario sin paquetes comprados', () {
    expect(usuarioNormal.paquetesComprados.length, 0);
  });
  test('Test modelo Usuario -> Usuario normal', () {
    expect(usuarioNormal.tipo, false);
  });
  test('Test modelo Usuario -> id user del usuario', () {
    expect(usuarioNormal.usuario, isNotNull);
  });
  test('Test modelo Usuario -> Imagen del usuario', () {
    expect(usuarioNormal.ventasTotales, 0);
  });
}
