import 'package:cuarentime_app/models/articulo.dart';
import 'package:cuarentime_app/models/categoria.dart';
import 'package:cuarentime_app/models/comentario.dart';
import 'package:cuarentime_app/models/etiqueta.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/usuario.dart';
import 'package:cuarentime_app/models/video.dart';

Usuario usuarioComprador = Usuario(
  id: "1",
  nombre: "Robinson Gonzalez",
  correo: "r.gonzalez10@mail.com",
  descripcion:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  paquetesComprados: [paquete1.id, paquete2.id],
  paquetesPublicados: [],
  articulosPublicados: [],
);

Usuario usuarioPublicador = Usuario(
  id: "2",
  nombre: "Mario Lopez",
  correo: "m.lopez@mail.com",
  descripcion:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  paquetesComprados: [],
  paquetesPublicados: [paquete1.id, paquete2.id],
  articulosPublicados: [articulo1.id, articulo2.id],
);

Categoria categoria1 = Categoria(
  id: "1",
  nombre: "Educacion",
  imagen: "",
);

Categoria categoria2 = Categoria(
  id: "2",
  nombre: "Musica",
  imagen: "",
);

Categoria categoria3 = Categoria(
  id: "3",
  nombre: "Deportes",
  imagen: "",
);

Categoria categoria4 = Categoria(
  id: "4",
  nombre: "Cocina",
  imagen: "",
);

Categoria categoria5 = Categoria(
  id: "5",
  nombre: "Ecologia",
  imagen: "",
);

Categoria categoria6 = Categoria(
  id: "6",
  nombre: "Tecnologia",
  imagen: "",
);

Etiqueta etiqueta1 = Etiqueta(
  id: "1",
  titulo: "yoga",
);

Etiqueta etiqueta2 = Etiqueta(
  id: "2",
  titulo: "ejercicio",
);

Etiqueta etiqueta3 = Etiqueta(
  id: "3",
  titulo: "musica",
);

Etiqueta etiqueta4 = Etiqueta(
  id: "4",
  titulo: "educacion",
);

Comentario comentario1 = Comentario(
  id: "1",
  comentario: "Muy buenos videos, vale la pena",
  usuario: "cristobal@mail.com",
  paqueteId: "1",
);

Comentario comentario2 = Comentario(
  id: "2",
  comentario: "Me sirvio muchas gracias :D",
  usuario: "diego@mail.com",
  paqueteId: "2",
);

Video video1 = Video(
  id: "1",
  titulo: "Clase numero 1",
  video: "",
  descripcion:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  imagen: "",
);

Video video2 = Video(
  id: "2",
  titulo: "Clase numero 2",
  video: "",
  descripcion:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  imagen: "",
);

Paquete paquete1 = Paquete(
  id: "1",
  titulo: "Paquete 1",
  imagen: "",
  resumen:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  videos: [video1.id, video2.id],
  likes: 20,
  comentarios: [comentario1.id, comentario2.id],
  etiquetas: [etiqueta1.id, etiqueta2.id],
  categoria: "1",
  usuario: "2",
);

Paquete paquete2 = Paquete(
  id: "2",
  titulo: "Paquete 2",
  imagen: "",
  resumen:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  videos: [video1.id, video2.id],
  likes: 20,
  comentarios: [comentario1.id, comentario2.id],
  etiquetas: [etiqueta3.id, etiqueta4.id],
  categoria: "2",
  usuario: "2",
);

Articulo articulo1 = Articulo(
  id: "1",
  titulo: "Articulo 1",
  contenido: "descripcion",
  imagen: "assets/images/home/cuentos.jpg",
  categoria: "1",
  usuario: "2",
);

Articulo articulo2 = Articulo(
  id: "2",
  titulo: "Articulo 2",
  contenido: "descripcion",
  imagen: "assets/images/home/yoga.jpg",
  categoria: "1",
  usuario: "2",
);
