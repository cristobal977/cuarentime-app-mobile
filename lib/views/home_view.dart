import 'package:cuarentime_app/animation/fade_animation.dart';
import 'package:cuarentime_app/models/articulo.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/views/see_article_view.dart';
import 'package:cuarentime_app/views/see_package_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  List<Articulo> articulos;
  List<Paquete> paquetes;
  ServicioFirestore _servicioFirestore;
  String palabra = "1000";
  var f = NumberFormat('#,###');

  @override
  void initState() {
    super.initState();
    _servicioFirestore = ServicioFirestore();
    _servicioFirestore.fetchArticulos().then((listado) {
      setState(() {
        if (listado.length > 0) {
          this.articulos = listado;
        }
      });
    });
    _servicioFirestore.fetchPaquetesVideos().then((listado) {
      if (listado.length > 0) {
        setState(() {
          this.paquetes = listado;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            'Cuarentime',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
          ),
          backgroundColor: Color(0xFF4090AA),
          bottom: TabBar(
            tabs: [
              Tab(text: "Paquetes"),
              Tab(text: "Articulos"),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            buildTabPaquetes(context),
            buildTabArticulos(context),
          ],
        ),
      ),
    );
  }

  SingleChildScrollView buildTabArticulos(BuildContext context) {
    return SingleChildScrollView(
      child: FadeAnimation(
        0.5,
        articulos != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List.generate(articulos.length, (index) {
                  return Container(
                    margin: EdgeInsets.only(
                        bottom: 10, left: 15, right: 15, top: 10),
                    padding: EdgeInsets.all(10),
                    decoration: buildBoxShadow(Colors.white),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                SeeArticleView(articulos[index]),
                          ),
                        );
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 700,
                            height: 200,
                            child: articulos[index].imagen != null
                                ? Image.network(
                                    (articulos[index].imagen),
                                    fit: BoxFit.fill,
                                  )
                                : Image.asset(
                                    "assets/images/home/No-disponible.jpg",
                                    fit: BoxFit.fill,
                                  ),
                          ),
                          ListTile(
                            title: Text(articulos[index].titulo),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              )
            : buildCargando(),
      ),
    );
  }

  SingleChildScrollView buildTabPaquetes(BuildContext context) {
    return SingleChildScrollView(
      child: FadeAnimation(
        0.5,
        paquetes != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List.generate(
                  paquetes.length,
                  (index) {
                    return Container(
                      margin: EdgeInsets.only(
                          bottom: 10, left: 15, right: 15, top: 10),
                      padding: EdgeInsets.all(10),
                      decoration: buildBoxShadow(Colors.white),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  SeePackageView(paquetes[index]),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 700,
                              height: 200,
                              child: paquetes[index].imagen != null &&
                                      paquetes[index].imagen != ""
                                  ? Image.network(
                                      (paquetes[index].imagen),
                                      fit: BoxFit.fill,
                                    )
                                  : Image.asset(
                                      "assets/images/home/No-disponible.jpg",
                                      fit: BoxFit.fill,
                                    ),
                            ),
                            ListTile(
                              title: Text(paquetes[index].titulo),
                              subtitle: Text("\$ " +
                                  f.format(int.parse(paquetes[index].precio))),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              )
            : buildCargando(),
      ),
    );
  }

  Container buildCargando() {
    return Container(
      height: 200,
      margin: EdgeInsets.only(bottom: 10, left: 15, right: 15, top: 10),
      padding: EdgeInsets.all(10),
      decoration: buildBoxShadow(Colors.white),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  BoxDecoration buildBoxShadow(Color color) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    );
  }
}
