import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/models/articulo.dart';
import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';

class SeeArticleView extends StatefulWidget {
  final Articulo articulo;
  SeeArticleView(this.articulo);

  @override
  _SeeArticleViewState createState() => _SeeArticleViewState();
}

class _SeeArticleViewState extends State<SeeArticleView> {
  List listaUsuarios;
  String nombreUsuario;
  String imagenUsuario;

  @override
  void initState() {
    super.initState();
    fetchUsuarios();
  }

  void fetchUsuarios() {
    CollectionReference collectionReference =
        Firestore.instance.collection('usuarios');
    collectionReference.snapshots().listen((snapshot) {
      setState(() {
        listaUsuarios = snapshot.documents;
        filterUser(widget.articulo.usuario);
      });
    });
  }

  void filterUser(id) {
    for (var i = 0; i < listaUsuarios.length; i++) {
      if (listaUsuarios[i]['usuario'] == id) {
        nombreUsuario = listaUsuarios[i]['nombre'];
        imagenUsuario = listaUsuarios[i]['imagen'];
        return;
      }
    }
    nombreUsuario = "Nombre no disponible";
    imagenUsuario = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Articulo',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.share,
                color: Colors.white,
              ),
              onPressed: null)
        ],
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FadeAnimation(
              0.7,
              Container(
                height: 200,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: widget.articulo.imagen != null
                          ? NetworkImage(widget.articulo.imagen)
                          : AssetImage('assets/images/home/No-disponible.jpg'),
                      fit: BoxFit.fill),
                ),
              ),
            ),
            FadeAnimation(
              1.4,
              listaUsuarios != null
                  ? ListTile(
                      title: Text(widget.articulo.titulo),
                      subtitle: Text(nombreUsuario),
                      leading: CircleAvatar(
                        child: imagenUsuario != null
                            ? Image.network(
                                imagenUsuario,
                                fit: BoxFit.fill,
                              )
                            : Text("ND"),
                      ),
                    )
                  : Text("data"),
            ),
            FadeAnimation(
              1.8,
              Container(
                color: Colors.black,
                height: 200,
                child: Center(
                  child: Text(
                    "Publicidad",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            FadeAnimation(
              2.2,
              Container(
                  padding: EdgeInsets.all(10),
                  child: Text(widget.articulo.contenido)),
            ),
          ],
        ),
      ),
    );
  }
}
