import 'package:cuarentime_app/components/bottom_nav_bar.dart';
import 'package:cuarentime_app/components/login_background.dart';
import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';

class RegisterView extends StatefulWidget {
  RegisterView({Key key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              buildBackground(),
              buildFormRegister(context),
            ],
          ),
        ),
      ),
    );
  }

  Padding buildFormRegister(context) => Padding(
        padding:
            EdgeInsets.only(top: 1.0, bottom: 30.0, left: 30.0, right: 30.0),
        child: Column(
          children: <Widget>[
            FadeAnimation(
              1.8,
              Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(143, 148, 251, .2),
                      blurRadius: 20.0,
                      offset: Offset(0, 10),
                    )
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.grey[100],
                          ),
                        ),
                      ),
                      child: buildTextFieldLogin("Nombre", false, Icons.person),
                    ),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.grey[100],
                          ),
                        ),
                      ),
                      child: buildTextFieldLogin("Correo", false, Icons.mail),
                    ),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      child:
                          buildTextFieldLogin("Contraseña", true, Icons.lock),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 15),
            FadeAnimation(
              2,
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BottomNavBar(0)),
                  );
                },
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(64, 144, 170, 1),
                        Color.fromRGBO(64, 144, 170, .6),
                      ],
                    ),
                  ),
                  child: Center(
                    child: Text(
                      "Registrarse",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  TextField buildTextFieldLogin(String hintText, bool obscureText, icon) =>
      TextField(
        obscureText: obscureText,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hintText,
          icon: Icon(
            icon,
            color: Color(0xFF4090AA),
          ),
          hintStyle: TextStyle(
            color: Colors.grey[400],
          ),
        ),
      );
}
