import 'package:cuarentime_app/animation/fade_animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cuarentime_app/bloc/register_bloc/bloc.dart';
import 'package:cuarentime_app/bloc/authentication_bloc/bloc.dart';
import 'package:cuarentime_app/views/register/register_button.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  // Dos variables
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  RegisterBloc _registerBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {
      // Si estado es submitting
      if (state.isSubmitting) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Registering'),
                CircularProgressIndicator()
              ],
            ),
          ));
      }
      // Si estado es success
      if (state.isSuccess) {
        BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
        Navigator.of(context).pop();
      }
      // Si estado es failure
      if (state.isFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Registration Failure'),
                Icon(Icons.error)
              ],
            ),
            backgroundColor: Colors.red,
          ));
      }
    }, child: BlocBuilder<RegisterBloc, RegisterState>(
      builder: (context, state) {
        return Container(
            color: Colors.white10,
            child: Form(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 380,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                            'assets/images/login/background2.png',
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                      child: Stack(
                        children: [
                          buildBackgroundImage(30, null, null, 80, 200, 1,
                              "assets/images/login/light-1.png"),
                          buildBackgroundImage(140, null, null, 80, 150, 1.3,
                              "assets/images/login/light-2.png"),
                          buildBackgroundImage(null, null, 40, 80, 150, 1.5,
                              'assets/images/login/clock.png'),
                          Positioned(
                            left: 20,
                            top: 215,
                            right: null,
                            width: null,
                            height: null,
                            child: FadeAnimation(
                              1,
                              Container(
                                width: 320,
                                child: Image.asset(
                                  "assets/images/login/logo1.png",
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          FadeAnimation(
                            1.4,
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(143, 148, 251, .2),
                                    blurRadius: 20.0,
                                    offset: Offset(0, 10),
                                  )
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[100],
                                        ),
                                      ),
                                    ),
                                    child: TextFormField(
                                      controller: _emailController,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        icon: Icon(
                                          Icons.email,
                                          color: Color(0xFF4090AA),
                                        ),
                                        labelText: 'Correo',
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                      autocorrect: false,
                                      // ignore: deprecated_member_use
                                      autovalidate: true,
                                      validator: (_) {
                                        return !state.isEmailValid
                                            ? 'Invalid Email'
                                            : null;
                                      },
                                    ),
                                  ),

                                  // Un textForm para password
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _passwordController,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        icon: Icon(
                                          Icons.lock,
                                          color: Color(0xFF4090AA),
                                        ),
                                        labelText: 'Contraseña',
                                      ),
                                      obscureText: true,
                                      autocorrect: false,
                                      // ignore: deprecated_member_use
                                      autovalidate: true,
                                      validator: (_) {
                                        return !state.isPasswordValid
                                            ? 'Invalid Password'
                                            : null;
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // Un button
                          SizedBox(height: 15),
                          FadeAnimation(
                            1.8,
                            RegisterButton(
                              onPressed: isRegisterButtonEnabled(state)
                                  ? _onFormSubmitted
                                  : null,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ));
      },
    ));
  }

  void _onEmailChanged() {
    _registerBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _registerBloc.add(PasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _registerBloc.add(Submitted(
        email: _emailController.text, password: _passwordController.text));
  }
}

Positioned buildBackgroundImage(
  double left,
  double top,
  double right,
  double width,
  double height,
  double delay,
  String image,
) =>
    Positioned(
      left: left,
      top: top,
      right: right,
      width: width,
      height: height,
      child: FadeAnimation(
        delay,
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                image,
              ),
            ),
          ),
        ),
      ),
    );
