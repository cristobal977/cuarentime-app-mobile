import 'package:cuarentime_app/models/categoria.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/usuario2.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:cuarentime_app/views/package_purchase.view.dart';
import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';

class LibraryView extends StatefulWidget {
  @override
  _LibraryViewState createState() => _LibraryViewState();
}

class _LibraryViewState extends State<LibraryView> {
  ServicioFirestore _servicioFirestore = ServicioFirestore();
  String idUsuario;
  Usuario2 user;
  List<Paquete> paquetes;
  List<Categoria> categorias;
  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
        ServicioFirestore().fetchUsuario2(idUsuario).then((value) {
          setState(() {
            user = value;
            ServicioFirestore()
                .fetchPaquetesVideosComprados(user.paquetesComprados)
                .then((value) {
              setState(() {
                paquetes = value;
              });
            });
          });
        });
      });
    });
    ServicioFirestore().fetchCategorias().then((value) {
      setState(() {
        categorias = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          'Biblioteca',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: this.paquetes != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    FadeAnimation(
                      0.7,
                      Container(
                        margin: EdgeInsets.only(left: 16, bottom: 22, top: 12),
                        child: Text(
                          user != null
                              ? "Tienes ${this.paquetes.length} paquetes comprados"
                              : "0",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    FadeAnimation(
                        1.8,
                        this.paquetes.length > 0
                            ? Column(
                                children: List.generate(
                                  paquetes != null ? paquetes.length : 0,
                                  (index) => buildPurchasedPackge(
                                      paquetes[index], context),
                                ),
                              )
                            : Center(
                                child: Text("No tienes videos comprados :("),
                              )),
                  ],
                )
              : Center(
                  child: Container(
                  height: 128,
                  width: 128,
                  child: Center(child: CircularProgressIndicator()),
                )),
        ),
      ),
    );
  }

  InkWell buildPurchasedPackge(Paquete paquete, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PackagePurchaseView(paquete)),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15, left: 15, right: 15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: ListTile(
          title: Text(paquete.titulo),
          subtitle: Text(nombreCategoria(paquete.categoria)),
          leading: paquete.imagen != null
              ? Container(
                  height: 60,
                  width: 100,
                  child: Image.network(
                    paquete.imagen,
                    fit: BoxFit.fill,
                  ),
                )
              : Icon(Icons.play_arrow),
        ),
      ),
    );
  }

  String nombreCategoria(String idCategoria) {
    if (categorias != null) {
      for (var i = 0; i < categorias.length; i++) {
        if (idCategoria == categorias[i].id) {
          return categorias[i].nombre;
        }
      }
    }
    return "";
  }
}
