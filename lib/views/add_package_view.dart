import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/components/bottom_nav_bar.dart';
import 'package:cuarentime_app/models/categoria.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:flutter/material.dart';

class AddPackageView extends StatefulWidget {
  @override
  _AddPackageViewState createState() => _AddPackageViewState();
}

class _AddPackageViewState extends State<AddPackageView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String titulo = "";
  String resumen = "";
  String imagen = "";
  String precio = "2000";
  File _image;
  final picker = ImagePicker();
  List<Categoria> categorias;
  String categoria;
  String idUsuario;

  @override
  void initState() {
    super.initState();
    ServicioFirestore().fetchCategorias().then((value) {
      setState(() {
        categorias = value;
      });
    });
    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          'Agregar Paquete',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  "Formulario:",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              textFieldTitulo(),
              textFiledDescripcion(),
              textFieldPrecio(),
              Container(
                margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                decoration: buildBoxShadow(Colors.white),
                child: Container(
                  padding: EdgeInsets.only(left: 10),
                  child: DropdownButton(
                    hint: Text("Seleccione una Categorias",
                        style: TextStyle(fontSize: 20)),
                    value: categoria,
                    items: List.generate(
                        categorias != null ? categorias.length : 0, (index) {
                      return DropdownMenuItem(
                        child: Text(categorias[index].nombre,
                            style: TextStyle(fontSize: 20)),
                        value: categorias[index].id,
                      );
                    }),
                    onChanged: (value) {
                      setState(() {
                        categoria = value;
                      });
                    },
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, bottom: 10, right: 10),
                child: _image == null
                    ? Text(
                        "Imagen: No seleccionada",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Imagen: Seleccionado",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          ),
                        ],
                      ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                height: 50,
                decoration: buildBoxShadow(Color(0xFF4090AA)),
                child: InkWell(
                  onTap: getImage,
                  child: Center(
                    child: Text(
                      "Agregar imagen",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 10, right: 10, top: 28, bottom: 30),
                height: 50,
                decoration: buildBoxShadow(Colors.pinkAccent),
                child: InkWell(
                  //
                  onTap: () {
                    if (titulo != "" &&
                        precio != "" &&
                        resumen != "" &&
                        _image != null) {
                      uploadStatusImage();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BottomNavBar(0)),
                      );
                    } else {
                      displaySnackBar(context);
                    }
                  },
                  child: Center(
                    child: Text(
                      "Listo",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void uploadStatusImage() async {
    if (titulo != "" && precio != "" && resumen != "" && _image != null) {
      var timeKey = DateTime.now();
      final StorageReference postImage =
          FirebaseStorage.instance.ref().child("images");

      final StorageUploadTask uploadTaskImage =
          postImage.child(timeKey.toString() + ".jpg").putFile(_image);
      var imageUrl =
          await (await uploadTaskImage.onComplete).ref.getDownloadURL();
      imagen = imageUrl.toString();
      saveToDatabase();
    }
  }

  void saveToDatabase() {
    final firestoreInstance = Firestore.instance;
    firestoreInstance.collection("paquetes").add({
      "titulo": titulo,
      "aprobado": "",
      "resumen": resumen,
      "imagen": imagen,
      "etiquetas": [],
      "usuario": idUsuario,
      "categoria": categoria,
      "publicacion": DateTime.now(),
      "comentarios": [],
      "videos": [],
      "likes": 0,
      "precio": precio,
      "publicado": false,
    }).then((value) {});
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  Container textFieldTitulo() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Titulo",
          ),
          onChanged: (value) {
            setState(() {
              titulo = value;
            });
          },
        ),
      ),
    );
  }

  Container textFiledDescripcion() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          keyboardType: TextInputType.multiline,
          maxLines: 3,
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Descripcion",
          ),
          onChanged: (value) {
            setState(() {
              resumen = value;
            });
          },
        ),
      ),
    );
  }

  Container textFieldPrecio() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          keyboardType: TextInputType.number,
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Precio del paquete",
          ),
          onChanged: (value) {
            setState(() {
              precio = value;
            });
          },
        ),
      ),
    );
  }

  displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(content: Text('Llene todos los campos'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  BoxDecoration buildBoxShadow(Color color) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    );
  }
}
