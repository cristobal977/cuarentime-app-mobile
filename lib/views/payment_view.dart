import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/models/paquete.dart';
//import 'package:cuarentime_app/models/usuario.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/servicio_pagos.dart';
import 'package:cuarentime_app/views/library_view.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class PaymentView extends StatefulWidget {
  final Paquete paquete;
  final String uidComprador;
  PaymentView({Key key, this.uidComprador, this.paquete}) : super(key: key);
  @override
  _PaymentViewState createState() => _PaymentViewState();
}

class _PaymentViewState extends State<PaymentView> {
  ServicioFirestore servicioFirestore = ServicioFirestore();
  ServicioPagos servicioPagos = ServicioPagos();
  String idVenta;
  // Usuario usuario;
  // @override
  // void initState() {
  //   super.initState();
  //   servicioFirestore
  //       .cargarDatosUsuario(widget.paquete.usuario)
  //       .then((usuario) {
  //     setState(() {
  //       this.usuario = usuario;
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Realizar pago")),
      body: Center(
        child: Card(
          child: Container(
              padding: EdgeInsets.all(12),
              child: Column(
                children: [
                  Text(
                    "${widget.paquete.titulo}",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Divider(),
                  Text("Total videos: ${widget.paquete.videos.length} videos"),
                  Text("Precio: \$${widget.paquete.precio}"),
                  RaisedButton.icon(
                      icon: Icon(Icons.credit_card),
                      label: Text("Ir a pagar"),
                      onPressed: () {
                        servicioPagos
                            .generarOrdenPago(
                                widget.paquete.id,
                                widget.uidComprador,
                                widget.paquete.usuario,
                                widget.paquete.titulo,
                                widget.paquete.precio,
                                "p.soto08@ufromail.cl")
                            .then((datosVenta) {
                          setState(() {
                            this.idVenta = datosVenta['idVenta'];
                          });
                          abrirNavegador(datosVenta['urlPago']);
                        });
                      }),
                  Center(
                    child: this.idVenta != null
                        ? StreamBuilder(
                            stream: Firestore.instance
                                .collection("ventas")
                                .document(idVenta)
                                .snapshots(),
                            builder: (context, snapshot) {
                              return snapshot.hasData
                                  ? snapshot.data['pagado']
                                      ? ListTile(
                                          title: Text(
                                              "El pago se ha hecho correctamente"),
                                          subtitle: Text("Ir a Biblioteca"),
                                          leading: Icon(
                                            Icons.done,
                                            color: Colors.green,
                                          ),
                                          trailing: IconButton(
                                              icon:
                                                  Icon(Icons.arrow_forward_ios),
                                              onPressed: () => Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          LibraryView()))),
                                        )
                                      : ListTile(
                                          title: Text(
                                              "Aún no has comprado este ítem"),
                                          subtitle: Text("Esperando el pago"),
                                          leading: Icon(
                                            Icons.warning,
                                            color: Colors.amber,
                                          ),
                                        )
                                  : Text("Venta no creada");
                            })
                        : Text("Será dirigido a WebPay"),
                  )
                ],
              )),
        ),
      ),
    );
  }

  abrirNavegador(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "No se pudo abrir";
    }
  }
}
