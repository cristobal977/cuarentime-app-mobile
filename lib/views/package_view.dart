import 'package:cuarentime_app/models/comentario.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/video.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/views/add_video_view.dart';
import 'package:cuarentime_app/views/video_view.dart';
import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';

class PackageView extends StatefulWidget {
  final Paquete paquete;

  PackageView(this.paquete);

  @override
  _PackageState createState() => _PackageState();
}

class _PackageState extends State<PackageView> {
  List<Video> videos;
  List<Comentario> comentarios;
  @override
  void initState() {
    super.initState();
    ServicioFirestore().fetchVideos(widget.paquete.id).then((value) {
      setState(() {
        videos = value;
      });
    });

    ServicioFirestore().fetchComentarios(widget.paquete.id).then((value) {
      setState(() {
        comentarios = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Paquete',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FadeAnimation(
                0.7,
                Container(
                  margin: EdgeInsets.only(left: 15, bottom: 5, top: 10),
                  child: Text(
                    widget.paquete.titulo,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              FadeAnimation(
                0.7,
                Center(
                  child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    color: Colors.black,
                    width: 350,
                    height: 150,
                    child: Image.network(
                      widget.paquete.imagen,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
              FadeAnimation(
                1.0,
                Container(
                  margin:
                      EdgeInsets.only(top: 10, bottom: 10, right: 15, left: 15),
                  child: Text(widget.paquete.resumen),
                ),
              ),
              FadeAnimation(
                1.2,
                Container(
                  margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                  child: Text(
                    videos != null
                        ? "Videos: " + videos.length.toString()
                        : "Videos: 0",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              FadeAnimation(
                1.4,
                Column(
                  children: List.generate(
                    videos != null ? videos.length : 0,
                    (index) {
                      return buildWatchVideo(videos[index]);
                    },
                  ),
                ),
              ),
              FadeAnimation(
                2.2,
                Container(
                  margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                  child: Text(
                    comentarios != null
                        ? "Comentarios del paquete: " +
                            comentarios.length.toString()
                        : "0",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              FadeAnimation(
                2.2,
                Column(
                  children: List.generate(
                    comentarios != null ? comentarios.length : 0,
                    (index) {
                      return buildWatchComent(comentarios[index]);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddVideoView(widget.paquete),
            ),
          );
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.pinkAccent,
      ),
    );
  }

  InkWell buildWatchVideo(Video video) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => VideoView(video)),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15, left: 15, right: 15),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: ListTile(
          title: Text(video.titulo),
          leading: Container(
            height: 50,
            width: 80,
            color: Colors.blue[200],
            child: video.imagen != null
                ? Image.network(
                    video.imagen,
                    fit: BoxFit.fill,
                  )
                : Image.asset(
                    "assets/images/home/No-disponible.jpg",
                    fit: BoxFit.fill,
                  ),
          ),
        ),
      ),
    );
  }

  Container buildWatchComent(Comentario comentario) {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 15, right: 15),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: ListTile(
        title: Text(comentario.usuario),
        subtitle: Text(comentario.comentario),
      ),
    );
  }
}
