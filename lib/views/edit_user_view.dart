import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/components/bottom_nav_bar.dart';
import 'package:cuarentime_app/models/usuario2.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class EditUserView extends StatefulWidget {
  EditUserView({Key key}) : super(key: key);

  @override
  _EditUserViewState createState() => _EditUserViewState();
}

class _EditUserViewState extends State<EditUserView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String idUsuario;
  Usuario2 user;
  String nombre;
  String imagen = "";
  File _image;
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
        ServicioFirestore().fetchUsuario2(idUsuario).then((value) {
          setState(() {
            user = value;
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          'Agregar Articulo',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              buildNewName(context),
              SizedBox(height: 10),
              buildViewImage(),
              buildNewImage(context),
            ],
          ),
        ),
      ),
    );
  }

  Container buildViewImage() {
    return Container(
      margin: EdgeInsets.all(10),
      height: 400,
      width: 400,
      color: Colors.lightBlue[100],
      child: user != null
          ? Image.network(
              imagen != "" ? imagen : user.imagen,
              fit: BoxFit.fill,
            )
          : Center(
              child: Text("Sin Imagen"),
            ),
    );
  }

  Row buildNewName(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 240,
          height: 50,
          margin: EdgeInsets.only(left: 10),
          decoration: buildBoxShadow(Colors.white),
          child: Container(
            padding: EdgeInsets.only(left: 10),
            child: TextField(
              style: TextStyle(fontSize: 20),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Nombre",
              ),
              onChanged: (value) {
                setState(() {
                  nombre = value;
                });
              },
            ),
          ),
        ),
        Container(
          width: 120,
          height: 50,
          margin: EdgeInsets.only(left: 10),
          decoration: buildBoxShadow(Colors.pinkAccent),
          child: InkWell(
            onTap: () {
              if (nombre != null) {
                editarNombre(user.id, nombre);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BottomNavBar(0)),
                );
              }
            },
            child: Center(
              child: Text(
                "Cambiar",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Container buildNewImage(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(left: 10, right: 10),
      decoration: buildBoxShadow(Colors.pinkAccent),
      child: InkWell(
        onTap: getImage,
        child: Center(
          child: Text(
            "Cambiar imagen",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  BoxDecoration buildBoxShadow(Color color) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    );
  }

  Future<void> editarNombre(String docID, nombre) async {
    await Firestore.instance
        .collection("usuarios")
        .document(docID)
        .updateData({"nombre": nombre}).then((value) {});
  }

  Future<void> editarFoto(String docID, url) async {
    await Firestore.instance
        .collection("usuarios")
        .document(docID)
        .updateData({"imagen": url}).then((value) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BottomNavBar(0)),
      );
    });
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _image = File(pickedFile.path);
      uploadStatusImage();
    });
  }

  void uploadStatusImage() async {
    if (_image != null) {
      var timeKey = DateTime.now();
      final StorageReference postImage =
          FirebaseStorage.instance.ref().child("images");
      final StorageUploadTask uploadTaskImage =
          postImage.child(timeKey.toString() + ".jpg").putFile(_image);
      var imageUrl =
          await (await uploadTaskImage.onComplete).ref.getDownloadURL();
      imagen = imageUrl.toString();
      editarFoto(user.id, imagen);
    }
  }

  displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(content: Text('Se cambio la imagen'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
