import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/components/bottom_nav_bar.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';

class AddArticleView extends StatefulWidget {
  @override
  _AddArticleViewState createState() => _AddArticleViewState();
}

class _AddArticleViewState extends State<AddArticleView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String contenido = "";
  String titulo = "";
  String imagen = "";
  File _image;
  final picker = ImagePicker();
  String idUsuario;
  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          'Agregar Articulo',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  "Formulario:",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              textFieldTitulo(),
              textFiledDescripcion(),
              Container(
                margin: EdgeInsets.only(left: 10, bottom: 10, right: 10),
                child: _image == null
                    ? Text(
                        "Imagen: No seleccionada",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Imagen: Seleccionado",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          ),
                        ],
                      ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                height: 50,
                decoration: buildBoxShadow(Color(0xFF4090AA)),
                child: InkWell(
                  onTap: getImage,
                  child: Center(
                    child: Text(
                      "Agregar imagen",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 10, right: 10, top: 28, bottom: 30),
                height: 50,
                decoration: buildBoxShadow(Colors.pinkAccent),
                child: InkWell(
                  //
                  onTap: () {
                    if (titulo != "" && contenido != "" && _image != null) {
                      uploadStatusImage();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BottomNavBar(0)),
                      );
                    } else {
                      displaySnackBar(context);
                    }
                  },
                  child: Center(
                    child: Text(
                      "Listo",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void uploadStatusImage() async {
    if (titulo != "" && contenido != "" && _image != null) {
      var timeKey = DateTime.now();
      final StorageReference postImage =
          FirebaseStorage.instance.ref().child("images");
      final StorageUploadTask uploadTaskImage =
          postImage.child(timeKey.toString() + ".jpg").putFile(_image);
      var imageUrl =
          await (await uploadTaskImage.onComplete).ref.getDownloadURL();
      imagen = imageUrl.toString();
      saveToDatabase();
    }
  }

  void saveToDatabase() {
    final firestoreInstance = Firestore.instance;
    firestoreInstance.collection("articulos").add({
      "titulo": titulo,
      "contenido": contenido,
      "imagen": imagen,
      "usuario": idUsuario,
    }).then((value) {});
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(content: Text('Llene todos los campos'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Container textFieldTitulo() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Titulo",
          ),
          onChanged: (value) {
            setState(() {
              titulo = value;
            });
          },
        ),
      ),
    );
  }

  Container textFiledDescripcion() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          keyboardType: TextInputType.multiline,
          maxLines: 3,
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Contenido",
          ),
          onChanged: (value) {
            setState(() {
              contenido = value;
            });
          },
        ),
      ),
    );
  }

  BoxDecoration buildBoxShadow(Color color) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    );
  }
}
