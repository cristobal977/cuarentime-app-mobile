import 'package:cuarentime_app/animation/fade_animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cuarentime_app/bloc/login_bloc/bloc.dart';
import 'package:cuarentime_app/bloc/authentication_bloc/bloc.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:cuarentime_app/views/login/create_account_button.dart';
import 'package:cuarentime_app/views/login/google_login_button.dart';
import 'package:cuarentime_app/views/login/login_button.dart';

class LoginForm extends StatefulWidget {
  final UserRepository _userRepository;

  LoginForm({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;
  UserRepository get _userRepository => widget._userRepository;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        // tres casos, tres if:
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Login Failure'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Logging in... '),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Container(
            color: Colors.white10,
            child: Form(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 380,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                            'assets/images/login/background2.png',
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                      child: Stack(
                        children: [
                          buildBackgroundImage(30, null, null, 80, 200, 1,
                              "assets/images/login/light-1.png"),
                          buildBackgroundImage(140, null, null, 80, 150, 1.3,
                              "assets/images/login/light-2.png"),
                          buildBackgroundImage(null, null, 40, 80, 150, 1.5,
                              'assets/images/login/clock.png'),
                          Positioned(
                            left: 20,
                            top: 215,
                            right: null,
                            width: null,
                            height: null,
                            child: FadeAnimation(
                              1,
                              Container(
                                width: 320,
                                child: Image.asset(
                                  "assets/images/login/logo1.png",
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          FadeAnimation(
                            1.4,
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(143, 148, 251, .2),
                                    blurRadius: 20.0,
                                    offset: Offset(0, 10),
                                  )
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey[100],
                                        ),
                                      ),
                                    ),
                                    child: TextFormField(
                                      controller: _emailController,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        icon: Icon(
                                          Icons.email,
                                          color: Color(0xFF4090AA),
                                        ),
                                        labelText: 'Correo',
                                        hintStyle: TextStyle(
                                          color: Colors.grey[400],
                                        ),
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                      // ignore: deprecated_member_use
                                      autovalidate: true,
                                      autocorrect: false,
                                      validator: (_) {
                                        return !state.isEmailValid
                                            ? 'El correo no cumple con los requerimientos'
                                            : null;
                                      },
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _passwordController,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        icon: Icon(
                                          Icons.lock,
                                          color: Color(0xFF4090AA),
                                        ),
                                        labelText: 'Contraseña',
                                        hintStyle: TextStyle(
                                          color: Colors.grey[400],
                                        ),
                                      ),
                                      obscureText: true,
                                      // ignore: deprecated_member_use
                                      autovalidate: true,
                                      autocorrect: false,
                                      validator: (_) {
                                        return !state.isPasswordValid
                                            ? 'La contraseña no cumple con los requerimientos'
                                            : null;
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                // Tres botones:
                                // LoginButton
                                FadeAnimation(
                                  2,
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        LoginButton(
                                          onPressed: isLoginButtonEnabled(state)
                                              ? _onFormSubmitted
                                              : null,
                                        ),
                                        CreateAccountButton(
                                          userRepository: _userRepository,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                // CreateAccountButton

                                SizedBox(height: 10),
                                // GoogleLoginButton
                                FadeAnimation(
                                  2.4,
                                  GoogleLoginButton(),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _loginBloc.add(PasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _loginBloc.add(LoginWithCredentialsPressed(
        email: _emailController.text, password: _passwordController.text));
  }
}

Positioned buildBackgroundImage(
  double left,
  double top,
  double right,
  double width,
  double height,
  double delay,
  String image,
) =>
    Positioned(
      left: left,
      top: top,
      right: right,
      width: width,
      height: height,
      child: FadeAnimation(
        delay,
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                image,
              ),
            ),
          ),
        ),
      ),
    );
