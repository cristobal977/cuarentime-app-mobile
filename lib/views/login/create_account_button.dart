import 'package:flutter/material.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:cuarentime_app/views/register/register_screen.dart';

class CreateAccountButton extends StatelessWidget {
  final UserRepository _userRepository;

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(64, 144, 170, 1),
            Color.fromRGBO(64, 144, 170, .6),
          ],
        ),
      ),
      child: MaterialButton(
        minWidth: 155,
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return RegisterScreen(
                  userRepository: _userRepository,
                );
              },
            ),
          );
        },
        child: Center(
          child: Text(
            "Registrarse",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
