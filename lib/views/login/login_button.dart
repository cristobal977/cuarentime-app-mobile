import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final VoidCallback _onPressed;

  LoginButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(64, 144, 170, 1),
            Color.fromRGBO(64, 144, 170, .6),
          ],
        ),
      ),
      child: MaterialButton(
        minWidth: 155,
        onPressed: _onPressed,
        child: Center(
          child: Text(
            "Iniciar Sesion",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
