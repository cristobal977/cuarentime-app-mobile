import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromRGBO(64, 144, 170, 1),
              Color.fromRGBO(64, 144, 170, .6),
            ],
          ),
        ),
        child: Center(
          child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Image.asset("assets/images/login/logo1.png")),
        ),
      ),
    );
  }
}
