import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/video.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:cuarentime_app/views/payment_view.dart';
import 'package:cuarentime_app/views/video_view.dart';
import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';
import 'package:intl/intl.dart';

class SeePackageView extends StatefulWidget {
  final Paquete paquete;
  SeePackageView(this.paquete);

  @override
  _SeePackageViewState createState() => _SeePackageViewState();
}

class _SeePackageViewState extends State<SeePackageView> {
  List listaUsuarios;
  List listaCategorias;
  List listaComentarios;
  List listaVideos;
  List listaComentariosFiltrados;
  List listaVideosFiltrados;
  String nombreUsuario;
  String imagenUsuario;
  String nombreCategoria;
  String uid;
  var fecha;
  UserRepository _userRepository = UserRepository();
  ServicioFirestore _servicioFirestore = ServicioFirestore();
  bool paqueteComprado = false;
  var f = NumberFormat('#,###');

  @override
  void initState() {
    super.initState();
    _userRepository.getUid().then((uid) {
      setState(() {
        this.uid = uid;
        _servicioFirestore
            .paqueteYaComprado(uid, widget.paquete.id)
            .then((estaComprado) => this.paqueteComprado = estaComprado);
      });
    });

    fetchUsuarios();
    fetchCategorias();
    fechaFormat();
    fetchComentarios();
    fetchVideos();
  }

  void fetchUsuarios() {
    CollectionReference collectionReference =
        Firestore.instance.collection('usuarios');
    collectionReference.snapshots().listen((snapshot) {
      setState(() {
        listaUsuarios = snapshot.documents;

        filterUser(widget.paquete.usuario);
      });
    });
  }

  void filterUser(id) {
    for (var i = 0; i < listaUsuarios.length; i++) {
      if (listaUsuarios[i]['usuario'] == id) {
        nombreUsuario = listaUsuarios[i]['nombre'];
        imagenUsuario = listaUsuarios[i]['imagen'];
        return;
      }
    }
    nombreUsuario = "Nombre no disponible";
    imagenUsuario = null;
  }

  void fetchCategorias() {
    CollectionReference collectionReference =
        Firestore.instance.collection('categorias');
    collectionReference.snapshots().listen((snapshot) {
      setState(() {
        listaCategorias = snapshot.documents;
        filterCategoria(widget.paquete.categoria);
      });
    });
  }

  void filterCategoria(id) {
    for (var i = 0; i < listaCategorias.length; i++) {
      if (listaCategorias[i].documentID == id) {
        nombreCategoria = listaCategorias[i]['nombre'];
        return;
      }
    }
    nombreCategoria = "Categoria no asignada";
  }

  void fetchComentarios() {
    CollectionReference collectionReference =
        Firestore.instance.collection('comentarios');
    collectionReference.snapshots().listen((snapshot) {
      setState(() {
        listaComentarios = snapshot.documents;

        filterComentarios(widget.paquete.id);
      });
    });
  }

  void filterComentarios(id) {
    listaComentariosFiltrados = listaComentarios;
    for (var i = 0; i < listaComentarios.length; i++) {
      if (listaComentarios[i]["paqueteId"] != id) {
        listaComentariosFiltrados.remove(listaComentarios[i]);
      }
    }
  }

  void fetchVideos() {
    CollectionReference collectionReference =
        Firestore.instance.collection('videos');
    collectionReference.snapshots().listen((snapshot) {
      setState(() {
        listaVideos = snapshot.documents;
        filterVideos(widget.paquete.id);
      });
    });
  }

  void filterVideos(id) {
    listaVideosFiltrados = listaVideos;

    for (var i = 0; i < listaVideos.length; i++) {
      if (listaVideos[i]["paquete"] != id) {
        listaVideosFiltrados.remove(listaVideos[i]);
      }
    }
  }

  void fechaFormat() {
    var timestamp = widget.paquete.publicacion;
    var date =
        DateTime.fromMillisecondsSinceEpoch(timestamp.millisecondsSinceEpoch);
    fecha = DateFormat.yMMMd().format(date);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Paquete',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.share,
                color: Colors.white,
              ),
              onPressed: null)
        ],
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FadeAnimation(
            0.8,
            Container(
              height: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: widget.paquete.imagen != null
                        ? NetworkImage(widget.paquete.imagen)
                        : AssetImage('assets/images/home/No-disponible.jpg'),
                    fit: BoxFit.fill),
              ),
            ),
          ),
          FadeAnimation(
            1.2,
            listaUsuarios != null
                ? ListTile(
                    title: Text(widget.paquete.titulo),
                    subtitle: Text(nombreUsuario),
                    leading: CircleAvatar(
                      child: imagenUsuario != null
                          ? Image.network(
                              imagenUsuario,
                              fit: BoxFit.fill,
                            )
                          : Text("ND"),
                    ),
                  )
                : Text("data"),
          ),
          FadeAnimation(
            1.6,
            Container(
                padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Text(widget.paquete.resumen)),
          ),
          FadeAnimation(
            1.8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                listaCategorias != null
                    ? Container(
                        padding:
                            EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        child: Text(
                          "Categoria: " + nombreCategoria,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      )
                    : Text("data"),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Text(
                    "Publicado: " + fecha.toString(),
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
          FadeAnimation(
            2.0,
            Container(
              padding: EdgeInsets.only(left: 10, right: 10, bottom: 5),
              child: Text(
                listaCategorias != null
                    ? "Cantidad de videos: " +
                        listaVideosFiltrados.length.toString()
                    : "Cantidad de videos: 0",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          FadeAnimation(
            2.0,
            listaVideosFiltrados != null
                ? paqueteComprado
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(
                          listaVideosFiltrados.length,
                          (index) => ListTile(
                              onTap: () {},
                              dense: true,
                              title:
                                  Text(listaVideosFiltrados[index]["titulo"]),
                              leading: IconButton(
                                  icon: Icon(Icons.play_circle_outline),
                                  onPressed: () {})),
                        ),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(
                          listaVideosFiltrados.length,
                          (index) => ListTile(
                              onTap: () {},
                              dense: true,
                              title:
                                  Text(listaVideosFiltrados[index]["titulo"]),
                              leading: IconButton(
                                  icon: Icon(Icons.lock), onPressed: () {})),
                        ),
                      )
                : Container(
                    padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Text(
                      "No hay Videos disponibles",
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
          ),
          FadeAnimation(
            2.2,
            Container(
              padding: EdgeInsets.only(
                top: 10,
                left: 10,
                right: 10,
              ),
              child: Text(
                listaComentariosFiltrados != null
                    ? "Comentarios: " +
                        listaComentariosFiltrados.length.toString()
                    : "Comentarios: 0",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          // FadeAnimation(
          //   2.2,
          //   listaComentariosFiltrados != null
          //       ? ListView.builder(itemBuilder: (context, index) {
          //           return Container(
          //             margin:
          //                 EdgeInsets.only(bottom: 15, left: 15, right: 15),
          //             decoration: BoxDecoration(
          //               color: Colors.white,
          //               borderRadius: BorderRadius.circular(10),
          //               boxShadow: [
          //                 BoxShadow(
          //                   color: Colors.grey.withOpacity(0.5),
          //                   spreadRadius: 5,
          //                   blurRadius: 7,
          //                   offset:
          //                       Offset(0, 3), // changes position of shadow
          //                 ),
          //               ],
          //             ),
          //             child: ListTile(
          //               title: Text(listaVideos[index].titulo),
          //               leading: Container(
          //                 height: 50,
          //                 width: 80,
          //                 color: Colors.blue[200],
          //               ),
          //               trailing: IconButton(
          //                 icon: Icon(Icons.arrow_forward_ios),
          //                 onPressed: () {
          //                   Navigator.push(
          //                     context,
          //                     MaterialPageRoute(
          //                         builder: (context) =>
          //                             VideoView(listaVideos[index])),
          //                   );
          //                 },
          //               ),
          //             ),
          //           );
          //         })
          //       : Container(
          //           padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
          //           child: Text(
          //             "No hay comentarios disponibles",
          //             style: TextStyle(
          //               fontSize: 15,
          //             ),
          //           ),
          //         ),
          // ),
        ],
      )),
      // this.paqueteComprado ? Text("Comprado") : Text("No comprado")),
      bottomNavigationBar: this.paqueteComprado
          ? Container(width: 0, height: 0)
          : Container(
              height: 70,
              color: Color(0xFF4090AA),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Center(
                      child: Text(
                        "\$" +
                            f.format(
                              int.parse(widget.paquete.precio),
                            ),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 23,
                        ),
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 10),
                      child: MaterialButton(
                        minWidth: 200.0,
                        height: 45.0,
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => PaymentView(
                                    uidComprador: uid,
                                    paquete: widget.paquete))),
                        color: Colors.pinkAccent,
                        child: Text(
                          "Comprar",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                      ))
                ],
              ),
            ),
    );
  }

  Widget paqueteCard(Video video) {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 15, right: 15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: ListTile(
        title: Text(video.titulo),
        leading: Container(
          height: 50,
          width: 80,
          color: Colors.blue[200],
        ),
        trailing: IconButton(
          icon: Icon(Icons.arrow_forward_ios),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => VideoView(video)),
            );
          },
        ),
      ),
    );
  }
}

// ?
// // cuando no está comprado
// : Column(
//     crossAxisAlignment: CrossAxisAlignment.start,
//     children: [
//       FadeAnimation(
//         0.8,
//         Container(
//           height: 200,
//           decoration: BoxDecoration(
//             image: DecorationImage(
//                 image: widget.paquete.imagen != null
//                     ? NetworkImage(
//                         widget.paquete.imagen)
//                     : AssetImage(
//                         'assets/images/home/No-disponible.jpg'),
//                 fit: BoxFit.fill),
//           ),
//         ),
//       ),
//       FadeAnimation(
//         1.2,
//         listaUsuarios != null
//             ? ListTile(
//                 title: Text(widget.paquete.titulo),
//                 subtitle: Text(nombreUsuario),
//                 leading: CircleAvatar(
//                   child: imagenUsuario != null
//                       ? Image.network(
//                           imagenUsuario,
//                           fit: BoxFit.fill,
//                         )
//                       : Text("ND"),
//                 ),
//               )
//             : Text("data"),
//       ),
//       FadeAnimation(
//         1.6,
//         Container(
//             padding: EdgeInsets.only(
//                 left: 10, right: 10, bottom: 10),
//             child: Text(widget.paquete.resumen)),
//       ),
//       FadeAnimation(
//         1.8,
//         Row(
//           mainAxisAlignment:
//               MainAxisAlignment.spaceAround,
//           children: [
//             listaCategorias != null
//                 ? Container(
//                     padding: EdgeInsets.only(
//                         left: 10,
//                         right: 10,
//                         bottom: 10),
//                     child: Text(
//                       "Categoria: " + nombreCategoria,
//                       style: TextStyle(
//                         fontSize: 15,
//                       ),
//                     ),
//                   )
//                 : Text("data"),
//             Container(
//               padding: EdgeInsets.only(
//                   left: 10, right: 10, bottom: 10),
//               child: Text(
//                 "Publicado: " + fecha.toString(),
//                 style: TextStyle(
//                   fontSize: 15,
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//       FadeAnimation(
//         2.0,
//         Container(
//           padding: EdgeInsets.only(
//               left: 10, right: 10, bottom: 5),
//           child: Text(
//             listaCategorias != null
//                 ? "Cantidad de videos: " +
//                     listaVideosFiltrados.length
//                         .toString()
//                 : "Cantidad de videos: 0",
//             style: TextStyle(
//                 fontSize: 18,
//                 fontWeight: FontWeight.bold),
//           ),
//         ),
//       ),
//       FadeAnimation(
//         2.0,
//         listaVideos != null
//             ? ListView.builder(
//                 itemBuilder: (context, index) {
//                 return Container(
//                   margin: EdgeInsets.only(
//                       bottom: 15, left: 15, right: 15),
//                   decoration: BoxDecoration(
//                     color: Colors.white,
//                     borderRadius:
//                         BorderRadius.circular(10),
//                     boxShadow: [
//                       BoxShadow(
//                         color: Colors.grey
//                             .withOpacity(0.5),
//                         spreadRadius: 5,
//                         blurRadius: 7,
//                         offset: Offset(0,
//                             3), // changes position of shadow
//                       ),
//                     ],
//                   ),
//                   child: ListTile(
//                     title:
//                         Text(listaVideos[index].titulo),
//                     leading: Container(
//                       height: 50,
//                       width: 80,
//                       color: Colors.blue[200],
//                     ),
//                     trailing: IconButton(
//                       icon:
//                           Icon(Icons.arrow_forward_ios),
//                       onPressed: () {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) =>
//                                   VideoView(listaVideos[
//                                       index])),
//                         );
//                       },
//                     ),
//                   ),
//                 );
//               })
//             : Container(
//                 padding: EdgeInsets.only(
//                     left: 10, right: 10, bottom: 10),
//                 child: Text(
//                   "No hay Videos disponibles",
//                   style: TextStyle(
//                     fontSize: 15,
//                   ),
//                 ),
//               ),
//       ),
//       FadeAnimation(
//         2.2,
//         Container(
//           padding: EdgeInsets.only(
//             top: 10,
//             left: 10,
//             right: 10,
//           ),
//           child: Text(
//             listaComentariosFiltrados != null
//                 ? "Comentarios: " +
//                     listaComentariosFiltrados.length
//                         .toString()
//                 : "Comentarios: 0",
//             style: TextStyle(
//                 fontSize: 18,
//                 fontWeight: FontWeight.bold),
//           ),
//         ),
//       ),
//       FadeAnimation(
//         2.2,
//         listaComentariosFiltrados != null
//             ? Column(
//                 children: List.generate(
//                   listaComentariosFiltrados.length,
//                   (index) => ListTile(
//                     title: Text(
//                         listaComentariosFiltrados[index]
//                             ["usuario"]),
//                     subtitle: Text(
//                         listaComentariosFiltrados[index]
//                             ["comentario"]),
//                     leading: CircleAvatar(
//                       child: Text(
//                         listaComentariosFiltrados[index]
//                                 ["usuario"]
//                             .toString()
//                             .substring(0, 1),
//                       ),
//                     ),
//                   ),
//                 ),
//               )
//             : Container(
//                 padding: EdgeInsets.only(
//                     left: 10, right: 10, bottom: 10),
//                 child: Text(
//                   "No hay comentarios disponibles",
//                   style: TextStyle(
//                     fontSize: 15,
//                   ),
//                 ),
//               ),
//       ),
//     ],
//   );
