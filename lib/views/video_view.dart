import 'package:cuarentime_app/models/video.dart';
import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

class VideoView extends StatefulWidget {
  final Video video;
  VideoView(this.video);

  @override
  _VideoViewState createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;
  @override
  void initState() {
    super.initState();
    _videoPlayerController = VideoPlayerController.network(widget.video.video);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      aspectRatio: 16 / 9,
      autoPlay: true,
      looping: true,
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Video',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            child: Chewie(
              controller: _chewieController,
            ),
          ),
          SizedBox(height: 10),
          ListTile(
            title: Text(widget.video.titulo),
            leading: Container(
              height: 60,
              width: 100,
              child: widget.video.imagen != null
                  ? Image.network(
                      widget.video.imagen,
                      fit: BoxFit.fill,
                    )
                  : Image.asset(
                      "assets/images/home/No-disponible.jpg",
                      fit: BoxFit.fill,
                    ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(widget.video.descripcion),
          )
        ],
      )),
    );
  }
}
