import 'package:cuarentime_app/components/simple_account_menu.dart';
import 'package:cuarentime_app/models/articulo.dart';
import 'package:cuarentime_app/models/categoria.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/usuario2.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:cuarentime_app/views/add_article_view.dart';
import 'package:cuarentime_app/views/add_package_view.dart';
import 'package:cuarentime_app/views/article_view.dart';
import 'package:cuarentime_app/views/edit_user_view.dart';
import 'package:cuarentime_app/views/package_view.dart';
import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cuarentime_app/bloc/authentication_bloc/bloc.dart';
import 'package:intl/intl.dart';

class StudyView extends StatefulWidget {
  @override
  _StudyViewState createState() => _StudyViewState();
}

class _StudyViewState extends State<StudyView> {
  Usuario2 user;
  List<Paquete> paquetes;
  List<Articulo> articulos;
  List<Categoria> categorias;
  var f = NumberFormat('#,###');
  String idUsuario;
  String correo;
  @override
  void initState() {
    super.initState();

    UserRepository().getUser().then((value) {
      setState(() {
        correo = value;
      });
    });

    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
        ServicioFirestore().fetchUsuario2(idUsuario).then((value) {
          setState(() {
            user = value;
          });
        });
        ServicioFirestore().fetchPaquetesVideosSubidos(idUsuario).then((value) {
          setState(() {
            paquetes = value;
          });
        });
        ServicioFirestore().fetchArticulosSubidos(idUsuario).then((value) {
          setState(() {
            articulos = value;
          });
        });
      });
    });

    ServicioFirestore().fetchCategorias().then((value) {
      setState(() {
        categorias = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          'Estudio',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        actions: <Widget>[
          SimpleAccountMenu(
            icons: [
              Icon(Icons.exit_to_app),
              Icon(Icons.person),
            ],
            iconColor: Colors.white,
            onChange: (index) {
              if (index == 0) {
                BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
              }
              if (index == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EditUserView()),
                );
              }
            },
          ),
          SizedBox(width: 5)
        ],
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              FadeAnimation(
                1,
                user != null
                    ? Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(10),
                            height: 100,
                            width: 100,
                            color: Colors.blue,
                            child: user.imagen != null
                                ? Image.network(
                                    user.imagen,
                                    fit: BoxFit.fill,
                                  )
                                : Center(child: Text("Sin Imagen")),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                user.nombre != null
                                    ? user.nombre
                                    : "Sin nombre",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                correo != null ? correo : "correo",
                                style: TextStyle(fontSize: 17),
                              )
                            ],
                          ),
                        ],
                      )
                    : Center(
                        child: CircularProgressIndicator(),
                      ),
              ),
              FadeAnimation(
                1.4,
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  height: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.deepPurple,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Total ventas",
                            style: TextStyle(fontSize: 17, color: Colors.white),
                          ),
                          user != null
                              ? Text(
                                  user.ventasTotales != null
                                      ? user.ventasTotales.toString()
                                      : "0",
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )
                              : CircularProgressIndicator()
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Paquetes",
                            style: TextStyle(fontSize: 17, color: Colors.white),
                          ),
                          Text(
                            paquetes != null ? paquetes.length.toString() : "0",
                            style: TextStyle(
                                fontSize: 28,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Ganancias",
                            style: TextStyle(fontSize: 17, color: Colors.white),
                          ),
                          Text(
                            user != null
                                ? "\$ " + f.format(user.ganancias)
                                : "\$ 0",
                            style: TextStyle(
                                fontSize: 28,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              FadeAnimation(
                1.8,
                Container(
                  margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                  child: Text(
                    "Paquetes subidos",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              FadeAnimation(
                2,
                Column(
                  children: List.generate(
                    paquetes != null ? paquetes.length : 0,
                    (index) {
                      return buildPostedPackge(paquetes[index], context);
                    },
                  ),
                ),
              ),
              FadeAnimation(
                2.4,
                Container(
                  margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                  child: Text(
                    "Articulos subidos",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              FadeAnimation(
                2.8,
                Column(
                  children: List.generate(
                    articulos != null ? articulos.length : 0,
                    (index) {
                      return buildPostedArticule(articulos[index], context);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          InkWell(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddArticleView()),
              );
            },
            child: Container(
              height: 55,
              width: 55,
              decoration: BoxDecoration(
                color: Colors.pinkAccent,
                borderRadius: BorderRadius.all(Radius.circular(100)),
              ),
              child: Icon(
                Icons.bookmark,
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(height: 10),
          InkWell(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddPackageView()),
              );
            },
            child: Container(
              height: 55,
              width: 55,
              decoration: BoxDecoration(
                color: Colors.pinkAccent,
                borderRadius: BorderRadius.all(Radius.circular(100)),
              ),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  InkWell buildPostedPackge(Paquete paquete, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PackageView(paquete)),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15, left: 15, right: 15),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: ListTile(
          title: Text(paquete.titulo),
          subtitle: Text(nombreCategoria(paquete.categoria)),
          leading: paquete.imagen != null
              ? Container(
                  height: 60,
                  width: 100,
                  child: Image.network(
                    paquete.imagen,
                    fit: BoxFit.fill,
                  ),
                )
              : Container(
                  height: 60,
                  width: 100,
                  color: Colors.red[200],
                  child: Text("NN"),
                ),
        ),
      ),
    );
  }

  InkWell buildPostedArticule(Articulo articulo, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ArticleView(articulo)),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15, left: 15, right: 15),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: ListTile(
          title: Text(articulo.titulo),
          leading: Container(
            height: 60,
            width: 100,
            child: articulo.imagen != null
                ? Image.network(
                    articulo.imagen,
                    fit: BoxFit.fill,
                  )
                : Image.asset(
                    "assets/images/home/No-disponible.jpg",
                    fit: BoxFit.fill,
                  ),
          ),
        ),
      ),
    );
  }

  String nombreCategoria(String idCategoria) {
    if (categorias != null) {
      for (var i = 0; i < categorias.length; i++) {
        if (idCategoria == categorias[i].id) {
          return categorias[i].nombre;
        }
      }
    }
    return "";
  }
}
