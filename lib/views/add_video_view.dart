import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/components/bottom_nav_bar.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:file_picker/file_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class AddVideoView extends StatefulWidget {
  final Paquete paquete;
  AddVideoView(this.paquete);

  @override
  _AddVideoViewState createState() => _AddVideoViewState();
}

class _AddVideoViewState extends State<AddVideoView> {
  String titulo = "";
  String descripcion = "";
  String imagen = "";
  String video = "";
  File _image;
  File _video;
  final picker = ImagePicker();
  final pickervideo = ImagePicker();
  String idUsuario;

  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Agregar video',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  "Formulario:",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              textFieldTitulo(),
              textFiledDescripcion(),
              Container(
                margin: EdgeInsets.only(left: 10, bottom: 10, right: 10),
                child: _image == null
                    ? Text(
                        "Imagen: No seleccionada",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Imagen: Seleccionado",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          ),
                        ],
                      ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                height: 50,
                decoration: buildBoxShadow(Color(0xFF4090AA)),
                child: InkWell(
                  onTap: getImage,
                  child: Center(
                    child: Text(
                      "Agregar imagen",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, bottom: 10, right: 10),
                child: _video == null
                    ? Text(
                        "Video: no seleccionado",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    : Text(
                        "Video: Seleccionado",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.green,
                        ),
                      ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                height: 50,
                decoration: buildBoxShadow(Color(0xFF4090AA)),
                child: InkWell(
                  onTap: getvideo,
                  child: Center(
                    child: Text(
                      "Agregar video",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 10, right: 10, top: 28, bottom: 30),
                height: 50,
                decoration: buildBoxShadow(Colors.pinkAccent),
                child: InkWell(
                  onTap: () {
                    uploadStatusImage();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BottomNavBar(0)),
                    );
                  },
                  child: Center(
                    child: Text(
                      "Listo",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void uploadStatusImage() async {
    if (titulo != "" && descripcion != "" && _video != null && _image != null) {
      var timeKey = DateTime.now();
      final StorageReference postImage =
          FirebaseStorage.instance.ref().child("images");

      final StorageUploadTask uploadTaskImage =
          postImage.child(timeKey.toString() + ".jpg").putFile(_image);
      var imageUrl =
          await (await uploadTaskImage.onComplete).ref.getDownloadURL();
      imagen = imageUrl.toString();
      final StorageReference postVideo =
          FirebaseStorage.instance.ref().child("videos");
      final StorageUploadTask uploadTaskVideo =
          postVideo.child(timeKey.toString() + ".mp4").putFile(_video);
      var videoUrl =
          await (await uploadTaskVideo.onComplete).ref.getDownloadURL();
      video = videoUrl.toString();

      saveToDatabase();
    }
  }

  void saveToDatabase() {
    final firestoreInstance = Firestore.instance;
    firestoreInstance.collection("videos").add({
      "titulo": titulo,
      "descripcion": descripcion,
      "imagen": imagen,
      "video": video,
      "paquete": widget.paquete.id,
      "usuario": widget.paquete.usuario,
    }).then((value) {});
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  Future getvideo() async {
    final pickedFile = await FilePicker.getFile(type: FileType.video);
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _video = File(pickedFile.path);
    });
  }

  Container textFieldTitulo() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Titulo",
          ),
          onChanged: (value) {
            setState(() {
              titulo = value;
            });
          },
        ),
      ),
    );
  }

  Container textFiledDescripcion() {
    return Container(
      margin: EdgeInsets.only(bottom: 15, left: 10, right: 10),
      decoration: buildBoxShadow(Colors.white),
      child: Container(
        padding: EdgeInsets.only(left: 10),
        child: TextField(
          keyboardType: TextInputType.multiline,
          maxLines: 3,
          style: TextStyle(fontSize: 20),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Descripcion",
          ),
          onChanged: (value) {
            setState(() {
              descripcion = value;
            });
          },
        ),
      ),
    );
  }

  BoxDecoration buildBoxShadow(Color color) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 5,
          blurRadius: 7,
          offset: Offset(0, 3),
        ),
      ],
    );
  }
}
