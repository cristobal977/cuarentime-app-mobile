import 'package:cuarentime_app/animation/fade_animation.dart';
import 'package:cuarentime_app/models/articulo.dart';
import 'package:flutter/material.dart';

class ArticleView extends StatefulWidget {
  final Articulo articulo;
  ArticleView(this.articulo);

  @override
  _ArticleViewState createState() => _ArticleViewState();
}

class _ArticleViewState extends State<ArticleView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Articulo',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
        backgroundColor: Color(0xFF4090AA),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FadeAnimation(
                0.7,
                Container(
                  margin: EdgeInsets.only(left: 15, bottom: 5, top: 10),
                  child: Text(
                    widget.articulo.titulo,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              FadeAnimation(
                0.7,
                Center(
                  child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    color: Colors.black,
                    width: 350,
                    height: 150,
                    child: widget.articulo.imagen != null
                        ? Image.network(
                            widget.articulo.imagen,
                            fit: BoxFit.fill,
                          )
                        : Image.asset(
                            "assets/images/home/No-disponible.jpg",
                            fit: BoxFit.fill,
                          ),
                  ),
                ),
              ),
              FadeAnimation(
                1.0,
                Container(
                  margin:
                      EdgeInsets.only(top: 10, bottom: 10, right: 15, left: 15),
                  child: Text(widget.articulo.contenido),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
