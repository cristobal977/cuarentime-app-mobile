import 'package:equatable/equatable.dart';

//Recordatorio los eventos son entradas y los estados son salidas

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

// Tres eventos:

// Aplicacion iniciada
// Conectado
// Desconectado

class AppStarted extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {}

class LoggedOut extends AuthenticationEvent {}
