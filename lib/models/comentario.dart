import 'package:cloud_firestore/cloud_firestore.dart';

class Comentario {
  final String id, comentario, usuario, paqueteId;

  Comentario({this.id, this.comentario, this.usuario, this.paqueteId});

  factory Comentario.fromSnapshot(DocumentSnapshot snapshot) {
    return Comentario(
      id: snapshot.documentID,
      comentario: snapshot.data['comentario'],
      usuario: snapshot.data["usuario"],
      paqueteId: snapshot.data['paqueteId'],
    );
  }
}
