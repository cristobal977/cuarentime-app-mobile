import 'package:cloud_firestore/cloud_firestore.dart';

class Video {
  String id, titulo, descripcion, video, imagen, paquete, usuario;

  Video(
      {this.id,
      this.titulo,
      this.descripcion,
      this.imagen,
      this.video,
      this.paquete,
      this.usuario});

  factory Video.fromSnapshot(DocumentSnapshot snapshot) {
    return Video(
      id: snapshot.documentID,
      titulo: snapshot.data['titulo'],
      descripcion: snapshot.data['descripcion'],
      imagen: snapshot.data['imagen'],
      video: snapshot.data['video'],
      paquete: snapshot.data['paquete'],
      usuario: snapshot.data['usuario'],
    );
  }
}
