import 'package:cloud_firestore/cloud_firestore.dart';

class Usuario {
  final String id, nombre, correo, descripcion;
  //<- array de paquetes comprados
  final List<String> paquetesComprados;
  //<- array de paquetes publicados
  final List<String> paquetesPublicados;
  //<- array de articulos publicados
  final List<String> articulosPublicados;

  Usuario(
      {this.id,
      this.nombre,
      this.correo,
      this.descripcion,
      this.paquetesComprados,
      this.paquetesPublicados,
      this.articulosPublicados});

  factory Usuario.fromSnapshot(DocumentSnapshot snapshot) {
    return Usuario(
        id: snapshot.documentID,
        nombre: snapshot.data['nombre'],
        correo: snapshot.data['correo'],
        descripcion: snapshot.data['descripcion'],
        paquetesComprados: snapshot.data['paquetesComprados'] == null
            ? null
            : List.from(snapshot.data['paquetesComprados']),
        paquetesPublicados: snapshot.data['paquetesPublicados'] == null
            ? null
            : List.from(snapshot.data['paquetesPublicados']),
        articulosPublicados: snapshot.data['articulosPublicados'] == null
            ? null
            : List.from(snapshot.data['articulosPublicados']));
  }
}
