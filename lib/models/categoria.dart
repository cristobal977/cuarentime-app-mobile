import 'package:cloud_firestore/cloud_firestore.dart';

class Categoria {
  final String id, nombre, imagen;

  Categoria({this.id, this.nombre, this.imagen});

  factory Categoria.fromSnapshot(DocumentSnapshot snapshot) {
    return Categoria(
        id: snapshot.documentID,
        nombre: snapshot.data['nombre'],
        imagen: snapshot.data['imagen']);
  }
}
