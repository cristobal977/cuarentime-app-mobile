import 'package:cloud_firestore/cloud_firestore.dart';

class Paquete {
  final String id, titulo, resumen, imagen, precio, aprobado;
  final Timestamp publicacion;
  final List<String> etiquetas;
  final String usuario;
  final String categoria;
  final List<String> comentarios; //<- array de ids de los comentarios
  final List<String> videos; // array de ids de los Videos
  final int likes;
  final bool publicado;

  Paquete(
      {this.id,
      this.titulo,
      this.aprobado,
      this.resumen,
      this.imagen,
      this.etiquetas,
      this.usuario,
      this.categoria,
      this.publicacion,
      this.comentarios,
      this.videos,
      this.likes,
      this.precio,
      this.publicado});

  factory Paquete.fromSnapshot(DocumentSnapshot snapshot) {
    return Paquete(
      id: snapshot.documentID,
      aprobado: snapshot.data['aprobado'],
      titulo: snapshot.data['titulo'],
      resumen: snapshot.data['resumen'] ?? "",
      imagen: snapshot.data['imagen'],
      usuario: snapshot.data['usuario'],
      categoria: snapshot.data['categoria'],
      publicacion: snapshot.data['publicacion'] == ""
          ? Timestamp.now()
          : snapshot.data['publicacion'],
      comentarios: snapshot.data['comentarios'] == null
          ? null
          : List.from(snapshot.data['comentarios']),
      videos: snapshot.data['videos'] == null
          ? null
          : List.from(snapshot.data['videos']),
      likes: snapshot.data['likes'] ?? 0,
      precio: snapshot.data['precio'] ?? 0,
      publicado: snapshot.data['publicado'],
    );
  }
}
