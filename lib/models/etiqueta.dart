import 'package:cloud_firestore/cloud_firestore.dart';

class Etiqueta {
  final String id, titulo;

  const Etiqueta({this.id, this.titulo});

  static Etiqueta fromSnapshot(DocumentSnapshot snapshot) {
    return Etiqueta(
      id: snapshot.documentID,
      titulo: snapshot.data['titulo'],
    );
  }
}
