import 'package:cloud_firestore/cloud_firestore.dart';

class Usuario2 {
  final String id, nombre, imagen, usuario;
  final bool tipo;
  final List<String> paquetes;
  final List<String> paquetesComprados;
  final int ventasTotales;
  final int ganancias;

  Usuario2(
      {this.id,
      this.nombre,
      this.imagen,
      this.paquetes,
      this.paquetesComprados,
      this.tipo,
      this.usuario,
      this.ventasTotales,
      this.ganancias});

  factory Usuario2.fromSnapshot(DocumentSnapshot snapshot) {
    print(snapshot.documentID);
    return Usuario2(
        id: snapshot.documentID,
        nombre: snapshot.data['nombre'],
        imagen: snapshot.data['imagen'],
        tipo: snapshot.data['tipo'],
        usuario: snapshot.data['usuario'],
        ventasTotales: snapshot.data['ventas_totales'],
        paquetesComprados: snapshot.data['paquetesComprados'] == null
            ? null
            : List.from(snapshot.data['paquetesComprados']),
        paquetes: snapshot.data['paquetes'] == null
            ? null
            : List.from(snapshot.data['paquetes']),
        ganancias: snapshot.data['ganancias'] != null
            ? snapshot.data['ganancias']
            : 0);
  }
}
