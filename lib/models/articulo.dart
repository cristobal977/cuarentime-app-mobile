import 'package:cloud_firestore/cloud_firestore.dart';

class Articulo {
  String id, titulo, imagen, contenido;
  String usuario;
  String categoria;

  Articulo({
    this.id,
    this.titulo,
    this.imagen,
    this.contenido,
    this.usuario,
    this.categoria,
  });

  factory Articulo.fromSnapshot(DocumentSnapshot snapshot) {
    return Articulo(
      id: snapshot.documentID,
      titulo: snapshot.data['titulo'],
      imagen: snapshot.data['imagen'],
      contenido: snapshot.data['contenido'],
      usuario: snapshot.data['usuario'],
      categoria: snapshot.data['categoria'],
    );
  }
}
