import 'package:cloud_firestore/cloud_firestore.dart';

class Venta {
  final String id, comprador, fecha, paquete, vendedor, precio;
  Venta({
    this.id,
    this.comprador,
    this.fecha,
    this.paquete,
    this.vendedor,
    this.precio,
  });
  factory Venta.fromSnapshot(DocumentSnapshot snapshot) {
    return Venta(
      id: snapshot.documentID,
      comprador: snapshot.data['comprador'],
      fecha: snapshot.data['fecha'],
      paquete: snapshot.data['paquete'],
      vendedor: snapshot.data['vendedor'],
      precio: snapshot.data['precio'],
    );
  }
}
