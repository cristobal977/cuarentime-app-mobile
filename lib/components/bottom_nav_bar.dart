import 'package:cuarentime_app/models/usuario2.dart';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:cuarentime_app/services/user_repository.dart';
import 'package:cuarentime_app/views/home_view.dart';
import 'package:cuarentime_app/views/library_view.dart';
import 'package:cuarentime_app/views/study_view.dart';
import 'package:flutter/material.dart';

class BottomNavBar extends StatefulWidget {
  final int index;
  BottomNavBar(this.index);
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  String idUsuario;
  Usuario2 user;

  @override
  void initState() {
    super.initState();
    UserRepository().getUid().then((value) {
      setState(() {
        idUsuario = value;
        ServicioFirestore().fetchUsuario(idUsuario).then((value) {
          setState(() {
            user = value;
          });
        });
      });
    });
  }

  int _index = 0;
  @override
  Widget build(BuildContext context) {
    Widget child;
    switch (_index) {
      case 0:
        child = HomeView();
        break;
      case 1:
        child = LibraryView();
        break;
      case 2:
        child = StudyView();
        break;
    }
    return Scaffold(
      body: SizedBox.expand(child: child),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(8),
            topLeft: Radius.circular(8),
          ),
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 4),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8),
            topRight: Radius.circular(8),
          ),
          child: BottomNavigationBar(
            backgroundColor: Color(0xFF4090AA),
            unselectedItemColor: Colors.white,
            selectedItemColor: Colors.white,
            currentIndex: _index,
            onTap: (i) => setState(() => _index = i),
            showUnselectedLabels: true,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.explore,
                ),
                title: Text("Explorar"),
                // label: "Explorar",
              ),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.ondemand_video,
                  ),
                  title: Text("Biblioteca")
                  // label: "Biblioteca",
                  ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                ),
                title: Text("Estudio"),
                // label: "Estudio",
              )
            ],
          ),
        ),
      ),
    );
  }
}
