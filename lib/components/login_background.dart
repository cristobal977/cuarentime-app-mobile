import 'package:flutter/material.dart';
import 'package:cuarentime_app/animation/fade_animation.dart';

Container buildBackground() => Container(
      height: 380,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/images/login/background2.png',
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: Stack(
        children: [
          buildBackgroundImage(
              30, null, null, 80, 200, 1, "assets/images/login/light-1.png"),
          buildBackgroundImage(
              140, null, null, 80, 150, 1.3, "assets/images/login/light-2.png"),
          buildBackgroundImage(
              null, null, 40, 80, 150, 1.5, 'assets/images/login/clock.png'),
          Positioned(
            child: FadeAnimation(
              1.6,
              Container(
                margin: EdgeInsets.only(top: 50),
                child: Center(
                  child: Text(
                    "Cuarentime",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );

Positioned buildBackgroundImage(
  double left,
  double top,
  double right,
  double width,
  double height,
  double delay,
  String image,
) =>
    Positioned(
      left: left,
      top: top,
      right: right,
      width: width,
      height: height,
      child: FadeAnimation(
        delay,
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                image,
              ),
            ),
          ),
        ),
      ),
    );
