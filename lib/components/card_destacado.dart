import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CardDestacado extends StatelessWidget {
  final String urlImagen;
  final String nombrePackVideos;
  final String nombreCreador;
  final MaterialPageRoute paginaDetallePackVideo;

  CardDestacado(this.urlImagen, this.nombrePackVideos, this.nombreCreador,
      this.paginaDetallePackVideo);
  @override
  Widget build(BuildContext context) {
    return Center(
        child: CachedNetworkImage(
      imageUrl: urlImagen,
      imageBuilder: (context, imageProvider) => Container(
        height: MediaQuery.of(context).size.height / 2,
        width: MediaQuery.of(context).size.width * .8,
        decoration: BoxDecoration(
            color: Colors.indigoAccent,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12, blurRadius: 8, offset: Offset(0, 2))
            ],
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover)),
        child: Material(
          borderRadius: BorderRadius.circular(8),
          shadowColor: Colors.black,
          color: Colors.black.withOpacity(0.25),
          child: InkWell(
              onTap: () => Navigator.push(context, paginaDetallePackVideo),
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: <Widget>[
                        Text("Destacado",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Flexible(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(nombrePackVideos,
                                style: Theme.of(context).textTheme.headline2),
                            Text("Por $nombreCreador",
                                style: Theme.of(context).textTheme.caption)
                            // Text("[ NOMBRE PACK VIDEOS ]",
                            //     style: Theme.of(context).textTheme.headline2),
                            // Text("Por [ NOMBRE CREADOR ]",
                            //     style: Theme.of(context).textTheme.caption)
                          ],
                        ))
                      ],
                    )
                  ],
                ),
              )),
        ),
      ),
    )
        // Container(
        //     decoration: BoxDecoration(
        //         color: Colors.indigoAccent,
        //         borderRadius: BorderRadius.circular(16),
        //         boxShadow: [
        //           BoxShadow(
        //               color: Colors.black12,
        //               blurRadius: 8,
        //               offset: Offset(0, 2))
        //         ],
        //         image: DecorationImage(
        //             fit: BoxFit.fitHeight,
        //             image: AssetImage(
        //                 "lib/recursos/imagenes/paisaje_ejemplo.png"))),
        //     height: MediaQuery.of(context).size.height / 2,
        //     width: MediaQuery.of(context).size.width * .8,
        //     child: Padding(
        //         padding: EdgeInsets.all(16),
        //         child: Column(
        //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //           children: <Widget>[
        // Row(
        //   children: <Widget>[
        //     Text("Destacado",
        //         style: Theme.of(context).textTheme.bodyText2)
        //   ],
        // ),
        // Row(
        //   children: <Widget>[
        //     Flexible(
        //         child: Column(
        //       mainAxisAlignment: MainAxisAlignment.end,
        //       crossAxisAlignment: CrossAxisAlignment.start,
        //       children: <Widget>[
        //         Text("[ NOMBRE PACK VIDEOS ]",
        //             style: Theme.of(context)
        //                 .textTheme
        //                 .headline2),
        //         Text("Por [ NOMBRE CREADOR ]",
        //             style:
        //                 Theme.of(context).textTheme.caption)
        //       ],
        //     ))
        //   ],
        // )
        //           ],
        //         )))
        );
  }
}
