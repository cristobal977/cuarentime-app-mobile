import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CardPackVideo extends StatelessWidget {
  final String urlImagen;
  final String nombrePackVideos;
  final String nombreCreador;
  final MaterialPageRoute paginaDetallePackVideo;

  CardPackVideo(
      {this.urlImagen,
      this.nombrePackVideos,
      this.nombreCreador,
      this.paginaDetallePackVideo});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .32,
        width: MediaQuery.of(context).size.width * .56,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
                imageUrl: urlImagen,
                imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.cover,
                          ),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black54,
                                blurRadius: 4,
                                offset: Offset(0, 2))
                          ]),
                      height: MediaQuery.of(context).size.height * .24,
                      width: MediaQuery.of(context).size.width * .48,
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => print("tap"),
                        ),
                      ),
                    )),
            Padding(
              padding: EdgeInsets.only(top: 4),
              child: AutoSizeText(
                "[ NOMBRE PACK VIDEOS ]",
                style: Theme.of(context).textTheme.caption,
              ),
            )
          ],
        ));
  }
}
