import 'dart:convert';
import 'package:cuarentime_app/services/servicio_firestore.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as dev;

import 'package:crypto/crypto.dart';

class ServicioPagos {
  ServicioFirestore _firestore = ServicioFirestore();
  final String apiKey = "6B2007FC-671E-4A3C-A843-326LDDF9410F";
  final String secretKey = "4e34c141e847d71141e3d542d30ffd8d3932e8e9";
  final String uriCrearPago = "https://sandbox.flow.cl/api/payment/create";
  final String uriObtenerEstadoPago =
      "https://sandbox.flow.cl/api/payment/getStatus";
  String urlConfirmation =
      "https://us-central1-cuarentime-b5a8c.cloudfunctions.net/pagoConfirmado";
  String urlReturn =
      "https://us-central1-cuarentime-b5a8c.cloudfunctions.net/paginaRetorno";

  String firmarParametros(String parametros) {
    var key = utf8.encode(secretKey);
    var bytes = utf8.encode(parametros);
    var hmacSha256 = new Hmac(sha256, key);
    var digest = hmacSha256.convert(bytes);

    return digest.toString();
  }

  Future<Map> generarOrdenPago(String idPaquete, idComprador, idCreador, asunto,
      precio, emailCliente) async {
    Map datosPago = {"urlPago": "", "idVenta": ""};
    String idOrden = await _firestore.crearOrden({
      "subject": asunto,
      "amount": precio,
      "email": emailCliente,
      "idComprador": idComprador,
      "idCreador": idCreador,
      "idPaquete": idPaquete,
      "pagado": false,
      "fecha_creacion_orden": DateTime.now()
    });
    datosPago['idVenta'] = idOrden;
    //String urlPago;
    String parametrosOrdenados =
        "amount${precio}apiKey${apiKey}commerceOrder${idOrden}email${emailCliente}subject${asunto}urlConfirmation${urlConfirmation}urlReturn$urlReturn";
    String s = firmarParametros(parametrosOrdenados);

    try {
      var uriResponse = await http.post(
          uriCrearPago +
              "?amount=$precio&apiKey=$apiKey&commerceOrder=$idOrden&email=$emailCliente&subject=$asunto&urlConfirmation=$urlConfirmation&urlReturn=$urlReturn&s=$s",
          body: {
            "amount": precio.toString(),
            "apiKey": apiKey,
            "commerceOrder": idOrden,
            "email": emailCliente,
            "subject": asunto,
            "urlConfirmation": urlConfirmation,
            "urlReturn": urlReturn,
            "s": s
          });

      Map respuesta = json.decode(uriResponse.body);
      String token = respuesta['token'];

      await _firestore.agregarTokenAOrden(idOrden, token);

      datosPago['urlPago'] =
          "https://sandbox.flow.cl/app/web/pay.php?token=$token";
    } catch (e) {
      dev.log("Error");
    }
    return datosPago;
  }

  //Future<bool> obtenerEstadoPago(String tokenPago) async {}
}
