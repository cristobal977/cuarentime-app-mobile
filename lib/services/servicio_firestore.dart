import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cuarentime_app/models/articulo.dart';
import 'package:cuarentime_app/models/categoria.dart';
import 'package:cuarentime_app/models/comentario.dart';
import 'package:cuarentime_app/models/paquete.dart';
import 'package:cuarentime_app/models/usuario.dart';
import 'package:cuarentime_app/models/usuario2.dart';
import 'package:cuarentime_app/models/video.dart';

class ServicioFirestore {
  final Firestore _firestore = Firestore.instance;

  Future<List<Articulo>> fetchArticulos() async {
    List<Articulo> articulos = [];
    await _firestore.collection("articulos").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var articulo = Articulo.fromSnapshot(doc);
        articulos.add(articulo);
      }).toList();
    });
    return articulos;
  }

  Future<List<Articulo>> fetchArticulosSubidos(String idUser) async {
    List<Articulo> articulos = [];
    await _firestore.collection("articulos").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var articulo = Articulo.fromSnapshot(doc);
        if (articulo.usuario == idUser) {
          articulos.add(articulo);
        }
      }).toList();
    });
    return articulos;
  }

  Future<List<Paquete>> fetchPaquetesVideos() async {
    List<Paquete> paquetes = [];

    await _firestore.collection("paquetes").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var paquete = Paquete.fromSnapshot(doc);

        paquetes.add(paquete);
      }).toList();
    });
    return paquetes;
  }

  Future<List<Paquete>> fetchPaquetesVideosSubidos(String idUser) async {
    List<Paquete> paquetes = [];

    await _firestore.collection("paquetes").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var paquete = Paquete.fromSnapshot(doc);
        if (paquete.usuario == idUser) {
          paquetes.add(paquete);
        }
      }).toList();
    });
    return paquetes;
  }

  Future<List<Paquete>> fetchPaquetesVideosComprados(
      List<String> paquetesComprados) async {
    List<Paquete> paquetes = [];
    if (paquetesComprados.length != 0) {
      for (var i = 0; i < paquetesComprados.length; i++) {
        await _firestore
            .collection("paquetes")
            .getDocuments()
            .then((snapshots) {
          snapshots.documents.map((doc) {
            var paquete = Paquete.fromSnapshot(doc);
            if (paquete.id == paquetesComprados[i]) {
              paquetes.add(paquete);
            }
          }).toList();
        });
      }
    }
    return paquetes;
  }

  Future<List<Categoria>> fetchCategorias() async {
    List<Categoria> categorias = [];

    await _firestore.collection("categorias").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var categoria = Categoria.fromSnapshot(doc);
        categorias.add(categoria);
      }).toList();
    });
    return categorias;
  }

  Future<List<Comentario>> fetchComentarios(String idPaquete) async {
    List<Comentario> comentarios = [];

    await _firestore.collection("comentarios").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var comentario = Comentario.fromSnapshot(doc);
        if (comentario.paqueteId == idPaquete) {
          comentarios.add(comentario);
        }
      }).toList();
    });
    return comentarios;
  }

  Future<Paquete> fetchOnePaquete(String idPaquete) async {
    return _firestore
        .collection("paquetes")
        .document(idPaquete)
        .get()
        .then((snapshot) => Paquete.fromSnapshot(snapshot));
  }

  Future<List<Video>> fetchVideos(String idPaquete) async {
    List<Video> videos = [];
    await _firestore.collection("videos").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var video = Video.fromSnapshot(doc);
        if (video.paquete == idPaquete) {
          videos.add(video);
        }
      }).toList();
    });
    return videos;
  }

  estadoCompra(String idDocument) async {
    _firestore
        .collection("ventas")
        .document(idDocument)
        .snapshots()
        .listen((doc) {
      // ignore: unnecessary_statements
      doc.data['pagado'];
    });
  }

  Future<String> crearOrden(Map<String, dynamic> datos) async {
    String idDocumento;
    await _firestore
        .collection("ventas")
        .add(datos)
        .then((doc) => idDocumento = doc.documentID);
    return idDocumento;
  }

  Future<void> agregarTokenAOrden(String docID, String token) async {
    await _firestore
        .collection("ventas")
        .document(docID)
        .updateData({"token": token});
  }

  Future<Usuario> cargarDatosUsuario(String uid) async {
    Usuario usuario;
    await _firestore
        .collection("usuarios")
        .document(uid)
        .get()
        .then((doc) => usuario = Usuario.fromSnapshot(doc));
    return usuario;
  }

  Future<Usuario2> cargarDatosUsuario2(String uid) async {
    Usuario2 usuario;
    await _firestore
        .collection("usuarios")
        .document(uid)
        .get()
        .then((doc) => usuario = Usuario2.fromSnapshot(doc));
    return usuario;
  }

  Future<Usuario2> fetchUsuario(String uid) async {
    Usuario2 usuario = Usuario2.fromSnapshot(
        await _firestore.collection("usuarios").document(uid).get());
    return usuario;
  }

  Future<bool> paqueteYaComprado(String idUsuario, String idPaquete) async {
    List paquetesComprados;

    try {
      await _firestore
          .collection("usuarios")
          .document(idUsuario)
          .get()
          .then((snapshot) {
        paquetesComprados = snapshot.data[''] as List;
      });
      return paquetesComprados.contains(idPaquete);
    } catch (e) {
      return false;
    }
  }

  Future<Usuario2> fetchUsuario2(String uid) async {
    List<Usuario2> usuarios = [];
    Usuario2 usuario2 = Usuario2();
    await _firestore.collection("usuarios").getDocuments().then((snapshots) {
      snapshots.documents.map((doc) {
        var usuario = Usuario2.fromSnapshot(doc);
        usuarios.add(usuario);
      }).toList();
    });
    for (var i = 0; i < usuarios.length; i++) {
      if (usuarios[i].usuario == uid) {
        return usuarios[i];
      }
    }
    final firestoreInstance = Firestore.instance;
    firestoreInstance.collection("usuarios").add({
      "imagen": null,
      "nombre": "Nuevo Usuario",
      "paquetes": [],
      "paquetesComprados": [],
      "tipo": false,
      "usuario": uid,
      "ventas_totales": 0,
    }).then((value) {});
    return usuario2;
  }
}
